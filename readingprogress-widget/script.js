if(UserData !== null && typeof UserData === 'object') {
  var progressJS = {
    "defaults": {
      //color
      "color": "#003f72",
      //height
      "height": "3px",
      "top": 0,
      "bottom": 0,
      "left": 0,
      "right": 0,
      "zIndex": 9999,
      //bottom or top
      "ontop": true,
      //left to right
      "ltr": true,
      //element to use value
      "attach": false,
      //js style or css
      "css": false,
      //decimal numbers
      "round": false,
      //use only values
      "nobar": false
    },
    "start": function (configs = {}) {
      var progressJSelem;
      if(!configs.nobar) {
        //create element
        progressJSelem = document.createElement("div");
        //use css instead of js style
        progressJSelem.setAttribute('id', 'progressbarJS');
        //append to body
        document.body.appendChild(progressJSelem);
        //styles
        progressJSelem.style.position = 'fixed';
        progressJSelem.style.zIndex = '99999';
        progressJSelem.style.width = '0%';
        //configurable options
        //top or bottom
        configs.ontop ? progressJSelem.style.bottom = '0' : progressJSelem.style.top = progressJS.defaults.top;
        //ltr or rtl
        configs.ltr ? progressJSelem.style.right = '0' : progressJSelem.style.left = progressJS.defaults.left;
        //height
        configs.height ? progressJSelem.style.height = configs.height : progressJSelem.style.height = progressJS.defaults.height;
        //color
        configs.color ? progressJSelem.style.backgroundColor = configs.color : progressJSelem.style.backgroundColor = progressJS.defaults.color;
      }
      var attachElem =  progressJS.defaults.attach;
      var roundto = progressJS.defaults.round;
      //round to
      configs.round ? roundto = configs.round : roundto = 2;
      //attach
      configs.attach ? attachElem = document.querySelector(configs.attach) : false ;
      //scroll event
      document.addEventListener('scroll', function (e) {
        var maxHeight = document.body.scrollHeight;
        var sizeHeight = window.innerHeight;
        var scrolls = window.scrollY;
        var percentage = (scrolls / (maxHeight - sizeHeight)) * 100;
        if (!configs.nobar) {
          progressJSelem.style.width = percentage.toFixed(roundto) + "%";
        }
        if (attachElem) {
          attachElem.innerHTML = percentage.toFixed(roundto);
        }
      });

    }
  }

  var userConfig = {};

  if(UserData['kamRPcolor']) {
    userConfig['color'] = UserData['kamRPcolor'];
  }

  if(UserData['kamRPposition'] === "top") {
    userConfig['ontop'] = false;
  } else {
    userConfig['ontop'] = true;
  }

  if(UserData['kamRPzIndex']) {
    userConfig['zIndex'] = UserData['kamRPzIndex'];
  }

  if(UserData['kamRPltr'] === "ltr") {
    userConfig['ltr'] = false;
  } else {
    userConfig['ltr'] = true;
  }

  if(UserData['kamRPheight']) {
    userConfig['height'] = UserData['kamRPheight']+"px";
  }
  if(UserData['kamRPattach']) {
    userConfig['attach'] = UserData['kamRPattach'];
  }
  if(UserData['kamRPround'] && UserData['kamRPround'].indexOf('kamRPround') > -1) {
    userConfig['round'] = true;
  }
  if(UserData['kamRPnobar'] && UserData['kamRPnobar'].indexOf('kamRPnobar') > -1) {
    userConfig['nobar'] = true;
  }

  progressJS.start(userConfig);
}
