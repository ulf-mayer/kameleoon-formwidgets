if(UserData !== null && typeof UserData === 'object') { 
  var productType = UserData['popularProducts'];
  var amountProducts = UserData['amountProducts'] !== undefined ? UserData['amountProducts'] : 5;
  var minutesHistory = UserData['minutesHistory'] !== undefined ? UserData['minutesHistory'] : 43800;
  var excludeIDs = UserData['excludeIDs'] !== undefined ? UserData['excludeIDs'].split(",") : [];
  var productWidth = UserData['productWidth'] !== undefined ? UserData['productWidth'] : 5;
  var productClickGoal = UserData['productClickGoal'];
  var handleText = UserData['handleText'];
  var siteCode = Kameleoon.Internals.configuration.siteCode;

  function getAllProductCounters(callback) {
    // 1 month
    var url = 'https://api-product.kameleoon.com/allProductCounters?siteCode='+siteCode+'&minutesHistory='+minutesHistory;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.send();
    xhr.onreadystatechange = function(){
      if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
        var answer = xhr.responseText;
        if(answer !== undefined && answer.length > 2) {
          if(typeof callback === 'function') {
            callback(answer);
          }
        }
      }
    }
  }

  function getProductData(ids, callback) {
    var maxLength = 1983;
    if(Array.isArray(ids)) {
      ids = ids.join(',');
    }
    if(ids.length <= (maxLength-ids.length)) {
      var url = 'https://api-product.kameleoon.com/productData?siteCode='+siteCode+'&eans='+ids+'&all=true';
      var xhr = new XMLHttpRequest();
      xhr.open('GET', url);
      xhr.send();
      xhr.onreadystatechange = function() {
        if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
          var answer = xhr.responseText;
          if(answer !== undefined && answer.length > 2) {
            if(typeof callback === 'function') {
              callback(answer);
            }
          }
        }
      }
    }
  }

  function getDataDrawWidget() {
    getAllProductCounters(function(response) {
      response = JSON.parse(response);

      var inventory = [];
      for (prop in response) {
        var object = {};
        object['id'] = prop;
        object['v'] = response[prop]['v'];
        object['atcq'] = response[prop]['atcq'];
        object['bq'] = response[prop]['bq'];
        inventory.push(object);
      }

      // remove from array if exclude id is within it
      for (var j = 0; j < inventory.length; j++) {
        for (var i = 0; i < excludeIDs.length; i++) {
          if(inventory[j]['id'] == excludeIDs[i].toString()) {
            inventory.splice(j, 1);
          }
        }
      }

      // could be done better?
      if(productType == "viewedProducts") {
        inventory.sort(function(a,b){
            return  b.v - a.v;
        });
      } else if (productType == "addedToCartProducts") {
        inventory.sort(function(a,b){
            return  b.atcq - a.atcq;
        });
      } else if (productType == "boughtProducts") {
        inventory.sort(function(a,b){
            return  b.bq - a.bq;
        });
      }


      var dataForIDs = [];
      for (var i = 0; i < amountProducts; i++) {
        dataForIDs.push(inventory[i]['id']);
      }

      dataForIDs = JSON.stringify(dataForIDs);
      getProductData(dataForIDs, function(response) {

        var topProducts = JSON.parse(response);
        var htmlWidget = "<div id='productWidgetHandle' onClick='document.querySelector(\"#productWidget\").classList.toggle(\"expanded\")'><span>"+handleText+"</span></div>";

        var hiddenArea = "margin-right: -675px;";
        var style = "max-width: 130px;";

        if(productWidth !== undefined) {
          style = "max-width: "+productWidth+"px;";
          var hiddenRight = (amountProducts*productWidth)+(amountProducts*5);
          hiddenArea = "margin-right: -"+hiddenRight+"px;";
        }

        for (product in topProducts) {

          var image = '';
          if(topProducts[product]['pictureHref'] !== undefined) {
            image = "<img class='thumbImage' src='"+topProducts[product]['pictureHref']+"' /><br />";
          }

          var clickGoal = '';
          if(productClickGoal !== undefined) {
            clickGoal = "onclick='Kameleoon.API.processConversion("+productClickGoal+");'";
          }

          htmlWidget = htmlWidget+"<div class='product "+topProducts[product]['id']+"' style='"+style+"'><a href='"+topProducts[product]['href']+"' "+clickGoal+">"+image+" <strong>"+topProducts[product]['name']+"</strong></a></div>";
        }

        var productWidget = document.createElement("div");
        productWidget.setAttribute("id", "productWidget");
        productWidget.setAttribute("style", hiddenArea);
        productWidget.setAttribute("class", "fadeInRight");

        var innerProducts = document.createElement("div");
        innerProducts.setAttribute("class", "innerProducts");
        productWidget.appendChild(innerProducts);

        innerProducts.innerHTML = htmlWidget;

        document.body.appendChild(productWidget);
      });
    });
  }

  getDataDrawWidget();

}
