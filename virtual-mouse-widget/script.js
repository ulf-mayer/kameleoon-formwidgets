if(UserData !== null && typeof UserData === 'object') { 
  //@todo: session storage
  console.log(UserData['showAmount']);

  var inactivityTime = function () {
      var t;
      window.onload = resetTimer;
      if(UserData['onmousemove'] && UserData['onmousemove'].indexOf('onmousemove') > -1) {
        document.onmousemove = resetTimer;
      }
      if(UserData['onkeypress'] && UserData['onkeypress'].indexOf('onkeypress') > -1) {
        document.onkeypress = resetTimer;
      }
      if(UserData['onmousedown'] && UserData['onmousedown'].indexOf('onmousedown') > -1) {
        document.onmousedown = resetTimer; // touchscreen presses
      }
      if(UserData['ontouchstart'] && UserData['ontouchstart'].indexOf('ontouchstart') > -1) {
        document.ontouchstart = resetTimer; // touchscreen presses
      }
      if(UserData['onclick'] && UserData['onclick'].indexOf('onclick') > -1) {
        document.onclick = resetTimer; // touchscreen presses
      }
      if(UserData['onscroll'] && UserData['onscroll'].indexOf('onscroll') > -1) {
        document.onscroll = resetTimer; // touchscreen presses
      }
      if(UserData['onkeypress'] && UserData['onkeypress'].indexOf('onkeypress') > -1) {
        document.onkeypress = resetTimer; // touchscreen presses
      }

      function showWidget() {
          var mouseWidget = document.createElement("div");
          mouseWidget.setAttribute("id", "mouseWidget");

          var innerPointer = document.createElement("div");
          innerPointer.setAttribute("class", "icon");
          mouseWidget.appendChild(innerPointer);

          var divClasses = "";
          if(UserData['animation'] !== "") {
            divClasses = divClasses+"animated "+UserData['animation']+" ";
          }

          if(UserData['animationDelay'] !== "") {
            divClasses = divClasses+"delay-"+UserData['animationDelay']+"s ";
          }

          if(UserData['animationDuration'] === "infinite") {
            divClasses = divClasses+"infinite ";
          }

          if(UserData['cursorColor'] === "white") {
            divClasses = divClasses+"invert ";
          }

          if(UserData['animationSpeed'] !== "normal") {
            divClasses = divClasses+UserData['animationSpeed']+" ";
          }

          if(UserData['cursor'] !== "") {
            divClasses = divClasses+UserData['cursor']+" ";
          }
          var styles = "";
          if(UserData['circleColor'] !== "") {
            styles = styles+"background-color: "+UserData['circleColor']+"; ";
          }

          if(UserData['circleSize'] !== "") {
            styles = styles+"width: "+UserData['circleSize']+"px; "+"height: "+UserData['circleSize']+"px; margin-left: -"+Math.round(UserData['circleSize']/2)+"px; margin-top: -"+Math.round(UserData['circleSize']/2)+"px;";
          }

          if(UserData['domElementSelector'] !== "") {
            var posX = document.querySelector(UserData['domElementSelector']).getBoundingClientRect().x;
            var posY = document.querySelector(UserData['domElementSelector']).getBoundingClientRect().y;
            var width = document.querySelector(UserData['domElementSelector']).getBoundingClientRect().width;
            var height = document.querySelector(UserData['domElementSelector']).getBoundingClientRect().height;
            styles = styles+"top: "+(posX+Math.round(height/2))+"px; "+"left: "+(posY+Math.round(width/2))+"px;";
          }

          if(styles !== "") {
            mouseWidget.setAttribute("style", styles);
          }

          if(divClasses !== "") {
            mouseWidget.setAttribute("class", divClasses);
          }

          Element.prototype.appendBefore = function (element) {
            element.parentNode.insertBefore(this, element);
          },false;

          Element.prototype.appendAfter = function (element) {
            element.parentNode.insertBefore(this, element.nextSibling);
          },false;

          if(UserData['lightbox'] && UserData['lightbox'].indexOf('lightbox') > -1) {
            var mouseWidgetOverlay = document.createElement("div");
            mouseWidgetOverlay.setAttribute("id", "mouseWidgetOverlay");
            document.body.insertBefore(mouseWidgetOverlay, document.body.firstChild);
            document.body.appendChild(mouseWidget);
          } else {
            document.body.appendChild(mouseWidget);
          }
        }

        function resetTimer() {
          clearTimeout(t);
          var mouseWidget = document.querySelector('#mouseWidget');
          if(mouseWidget !== null) {
            mouseWidget.parentNode.removeChild(mouseWidget);
          }
          var mouseWidgetOverlay = document.querySelector('#mouseWidgetOverlay');
          if(mouseWidgetOverlay !== null) {
            mouseWidgetOverlay.parentNode.removeChild(mouseWidgetOverlay);
          }
          t = setTimeout(showWidget, (UserData['idleTime']*1000))
        }
  };

  inactivityTime();
}
