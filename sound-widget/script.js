if(UserData !== null && typeof UserData === 'object') {
  function createPlayer() {
    var embed = document.createElement("audio");
    embed.classList.add("backgroundaudio");

    if(UserData['controls'] && UserData['controls'].indexOf('controls') > -1) {
      embed.setAttribute("controls", "controls");
    }
    if(UserData['autostart'] && UserData['autostart'].indexOf('autostart') > -1) {
      embed.setAttribute("autoplay", "autoplay");
    }
    if(UserData['hidden'] && UserData['hidden'].indexOf('hidden') > -1) {
     embed.classList.add("hidden");
    }
    if(UserData['mp3']) {
      var mp3audio = document.createElement("source");
      mp3audio.setAttribute("type", "audio/mp3");
      mp3audio.setAttribute("src", UserData['mp3']);
      embed.appendChild(mp3audio);
    }
    if(UserData['ogg']) {
      var oggaudio = document.createElement("source");
      oggaudio.setAttribute("type", "audio/ogg");
      oggaudio.setAttribute("src", UserData['ogg']);
      embed.appendChild(oggaudio);
    }
    document.body.appendChild(embed);
  }
  createPlayer();
}
