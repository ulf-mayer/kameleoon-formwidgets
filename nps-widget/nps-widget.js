Kameleoon.API.runWhenConditionTrue(function () {
  return typeof jQuery != "undefined" && jQuery("body").length > 0;
}, function () {
  if(UserData !== null && typeof UserData === 'object') {
    if (!Kameleoon.Utils.readLocalData("kCapping", "COOKIE")) {
      jQuery("body").prepend("<div id='kWidgetOverlay'></div>");
      jQuery("#kWidgetOverlay").prepend("<div id='kWidget'></div>");
      jQuery("#kWidget").append("<span class='h2'>"+UserData['question']+"</span>");
      jQuery("#kWidget").append("<div id='kNPS'></div>");

      if(UserData['showsmileys'] && UserData['showsmileys'].indexOf('showsmileys') > -1) {
        jQuery("#kNPS").addClass("smileys");
      }

      if(UserData['sticky'] && UserData['sticky'].indexOf('sticky') > -1) {
        jQuery("#kWidgetOverlay").addClass("sticky");
      }

      if(UserData['check'] && UserData['check'].indexOf('check') > -1) {
        jQuery("#kWidgetOverlay").addClass("check");
      }

      jQuery("#kWidgetOverlay").addClass(UserData['position']+' '+UserData['theme']);

      if(UserData['customCSS']) {
        jQuery("#kWidget").append("<style>"+UserData['customCSS']+"</style>");
      }

      jQuery("#kWidget").append("<div id='kClose'></div>");
      jQuery(document).on("mousedown", "#kClose" , function () {
        Kameleoon.API.processConversion(UserData['conversionAfterClose']);
      });
      jQuery("#kNPS").append("<span class='h3'>&nbsp;"+UserData['leastlikely']+"</span>");
      jQuery("#kNPS").append("<ul></ul>");
      for (i = 0; i < 11; i++) {
        jQuery("#kNPS ul").append("<li>" + i + "</li>");
      }
      jQuery("#kNPS").append("<span class='h3'>"+UserData['mostlikely']+"&nbsp;</span>");
      jQuery("body").on("mousedown", "#kNPS li", function () {
        jQuery("#kNPS li").removeClass("selected");
        jQuery(this).addClass("selected");
        Kameleoon.API.setCustomData("NPSWidget", jQuery(this).text());
        if(UserData['conversionAfterSubmit']) {
          Kameleoon.API.processConversion(UserData['conversionAfterSubmit']);
        }
        jQuery("#kWidget").html("<span class='h1'>"+UserData['thankyoutext']+"</span>");
        if(UserData['additionalstep'] && UserData['additionalstep'].indexOf('additionalstep') > -1) {
          setTimeout(secondQuestion, UserData['timeout']);
        } else {
            setTimeout(function () {
              jQuery("#kWidgetOverlay").remove();
              Kameleoon.Utils.createLocalData("kCapping", "true", "365", "", "COOKIE");
            }, UserData['thankyoutimeout']);
       }
      });

      if(UserData['additionalstep'] && UserData['additionalstep'].indexOf('additionalstep') > -1) {
        var secondQuestion = function () {
          jQuery("#kWidget").html("");
          jQuery("#kWidget").append("<span class='h2'>"+UserData['secondquestion']+"</span>");
          jQuery("#kWidget").append("<textarea placeholder='"+UserData['placeholderSecondQuestion']+"' autofocus></textarea>");
          jQuery("#kWidget").append("<br /><div id='kButton'>"+UserData['buttontext']+"</div>");
          jQuery("#kWidget").append("<div id='kClose'></div>");
          jQuery("body").on("mousedown", "#kButton", function () {
            Kameleoon.API.setCustomData("NPScomments", jQuery("#kWidget textarea").val());
            Kameleoon.API.processConversion(UserData['conversionAfterSecondSubmit']);
            jQuery("#kWidget").html("<span class='h1'>"+UserData['thankyoutext']+"</span>");
            setTimeout(function () {
              jQuery("#kWidgetOverlay").remove();
              Kameleoon.Utils.createLocalData("kCapping", "true", "365", "", "COOKIE");
            }, UserData['thankyoutimeout']);
          });
        }
      }

      jQuery("body").on("mousedown", "#kClose", function () {
        jQuery("#kWidgetOverlay").remove();
        Kameleoon.Utils.createLocalData("kCapping", "true", "", "", "COOKIE");
        Kameleoon.API.processConversion(UserData['conversionAfterClose']);
      });
    }
  }
}, 100);
