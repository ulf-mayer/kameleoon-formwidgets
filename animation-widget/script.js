if(UserData !== null && typeof UserData === 'object') {
  function animateElement() {
    var element, name, arr;
    element = document.querySelector(UserData.cssSelector);
    name = UserData.animation;
    arr = element.className.split(" ");
    if (arr.indexOf(name) == -1) {
        element.className += " " + name + " animated";
    }
   }
   animateElement();
}
