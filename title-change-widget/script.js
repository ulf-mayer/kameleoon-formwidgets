if(UserData !== null && typeof UserData === 'object') {
  var titleTag = document.getElementsByTagName("title")[0];
  var faviconTag = document.querySelectorAll('[rel="shortcut icon"]')[0];
  var interval = "";
  if(faviconTag !== undefined) {
    var originalFaviconSrc = faviconTag.getAttribute("href");
  }
  var originalTitleText = titleTag.innerHTML;
  if(UserData['keepOriginal'] && UserData['keepOriginal'].indexOf('keepOriginal') > -1) {
    alternativeTitle = UserData['title'] + ' - ' + originalTitleText;
  } else {
    alternativeTitle = UserData['title'];
  }
  if(document.addEventListener) document.addEventListener("visibilitychange", function () {
    if(document.hidden) {
      // Tab is inactive
      if(UserData['favicon'] !== "") {
        faviconTag.setAttribute("href", UserData['faviconTag']);
      }
      if(UserData['alternateTitle'] && UserData['alternateTitle'].indexOf('alternateTitle') > -1) {
        interval = setInterval(function(){
            if(titleTag.innerHTML === originalTitleText) {
            titleTag.innerHTML = alternativeTitle;
          } else {
            titleTag.innerHTML = originalTitleText;
          }
        }, 1000);
      } else {
          titleTag.innerHTML = alternativeTitle;
      }
    } else {
      // Tab is active again
      if(UserData['favicon'] !== "") {
        faviconTag.setAttribute("href", originalFaviconSrc);
      }

      if(UserData['alternateTitle'] && UserData['alternateTitle'].indexOf('alternateTitle') > -1) {
        clearInterval(interval);
      }
      titleTag.innerHTML = originalTitleText;
    }
  });
}
