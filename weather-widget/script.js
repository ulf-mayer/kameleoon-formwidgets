if(UserData !== null && typeof UserData === 'object') {
  var icon = "";
  var weatherID = "";
  var temperature = "";

  if(UserData['weatherType'] === "forecastWeather") {
    var forecastList = Kameleoon.Internals.runtime.weatherForecast.list;
    var daysInFuture = UserData['daysInFuture'];
    var timeOfDayInFuture = UserData['timeOfDayInFuture'];

    var d = new Date();
    d.setDate(d.getDate() + parseInt(daysInFuture));
    var futureDate = d.toISOString().substring(0, 10);
    // get weatherData from forecast
    for (var i = 0; i < forecastList.length; i++) {
      if(forecastList[i].dt_txt === (futureDate+' '+timeOfDayInFuture)) {
        icon = forecastList[i].weather[0].icon;
        weatherID = forecastList[i].weather[0].id;
        temperature = forecastList[i].main.temp;
      }
    }
    if(icon === "") {
      console.log("Unfortunately there is no forecast yet for your given date: "+futureDate+" "+timeOfDayInFuture);
    }
  } else {
    // current weather
    icon = Kameleoon.Internals.runtime.weather.weather[0].icon;
    weatherID = Kameleoon.Internals.runtime.weather.weather[0].id;
    temperature = Kameleoon.API.currentVisit.weather.temperature;
  }

  var weatherWidget = document.createElement("div");
  weatherWidget.setAttribute("id", "weatherWidget");

  var divClasses = "";
  if(UserData['showTemperature'] && UserData['showTemperature'].indexOf('showTemperature') > -1) {
    divClasses = divClasses+"temperature ";
  }

  if(UserData['showWeatherDescription'] && UserData['showWeatherDescription'].indexOf('showWeatherDescription') > -1) {
    divClasses = divClasses+"description ";
  }
  if(divClasses !== "") {
    weatherWidget.setAttribute("class", divClasses);
  }

  if(UserData['weatherType'] === "forecastWeather" && UserData['daysInFuture']) {
    var condition = "condition"+icon.substring(0, 2);
    if(UserData['showWeatherDescription'] && UserData['showWeatherDescription'].indexOf('showWeatherDescription') > -1) {
      var region = "";
      if(UserData['showRegion'] && UserData['showRegion'].indexOf('showRegion') > -1) {
        region = Kameleoon.API.currentVisit.geolocation.region;
      }
      var days =  document.createElement("div");
      days.setAttribute("class", "daysInFuture");
      days.innerText = UserData['textForecastBeforeDays']+" "+UserData['daysInFuture']+" "+UserData['textForecastAfterDays']+" "+UserData[condition]+" "+UserData['textForecastAfterWeather']+" "+region;
      weatherWidget.appendChild(days);
    }
  }
  var outerWeather = document.createElement("div");
  outerWeather.setAttribute("class", "outerWeather");

  var weather =  document.createElement("div");
  weather.setAttribute("class", "weatherIcon icon-"+icon+" weather-"+weatherID);
  var iconFragment = document.createElement("i");
  weather.appendChild(iconFragment);

  outerWeather.appendChild(weather);
  weatherWidget.appendChild(outerWeather);

  if(UserData['showTemperature'] && UserData['showTemperature'].indexOf('showTemperature') > -1) {
    var showTemperature = document.createElement("div");
    showTemperature.setAttribute("class", "showTemperature");

    /* Kelvin to Celsius */
    showTemperature.innerText = Math.round((300-temperature) * 10 ) / 10;
    weatherWidget.appendChild(showTemperature);
  }

  Element.prototype.appendBefore = function (element) {
    element.parentNode.insertBefore(this, element);
  },false;

  Element.prototype.appendAfter = function (element) {
    element.parentNode.insertBefore(this, element.nextSibling);
  },false;

  Element.prototype.replaceElement = function (element) {
    element.parentNode.replaceChild(this, element);
  },false;

  // If widget is used in the content of your pages or above
  if(UserData['positionPluginConfiguration'] === "INPAGE" && (UserData['domElementSelector'] !== "" || UserData['positionSelectorRelative'] !== "")) {
    var selector = document.querySelector(UserData['domElementSelector']);
    if(UserData['positionSelectorRelative'] === "BEFORE") {
      weatherWidget.appendBefore(selector);
    }
    if(UserData['positionSelectorRelative'] === "AFTER") {
      weatherWidget.appendAfter(selector);
    }
    if(UserData['positionSelectorRelative'] === "REPLACE") {
      weatherWidget.replaceElement(selector);
    }
  } else {
    document.body.appendChild(weatherWidget);
  }
}
